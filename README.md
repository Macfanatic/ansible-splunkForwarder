# Splunk Forwarder

## Function
Installs and configures the Splunk Universal Forwarder

## Vault
```
---
splunk_forward_server_URL: splunk.example.com
splunk_forward_server_port: 9997
splunk_forwarder_adminPass: password
```

## Tested Environment(s)
RHEL 7, CentOS 7, & macOS 10.12

## Changelog

**v1.0 (2016-09-01)**
* Original Release
